## Synopsis

**Proyecto 1** para la electiva de Robótica e Inteligencia Artificial de la UCAB del año 2015.

Consiste en un robot que busca una pelota y trata llevarla a otro extremo, superando algunos obstáculos. 

Desarrollado en **NXC C++**

## <a href="http://www.youtube.com/watch?v=UmiEnEr5yM8" target="_blank">Video Demostrativo</a>

<a href="http://www.youtube.com/watch?v=UmiEnEr5yM8" target="_blank">[![Proyecto Robótica](http://img.youtube.com/vi/UmiEnEr5yM8/0.jpg)]
(http://www.youtube.com/watch?v=UmiEnEr5yM8 "Video Title")</a>